var fs = require('fs');
var resemblejs = require('resemblejs');
Promise = require('promise');
var Execution = require('./../models/Execution');

var ResembleService = {};
var timestamp = (new Date()).getTime();

ResembleService.compare = (execution) => {
    const options = {};
	resemblejs(execution.beforeImgUri).compareTo(execution.afterImgUri).onComplete(function (data) {
		console.log(JSON.stringify(data));
        console.log("Creando Archivo de comparación ...");
		var filename = execution.beforeImgUri.replace(/^.*[\\\/]/, '')
		var comparationFileName = execution.comparationImgUri.replace('"', '\\') + 'comparation ' + filename;
		fs.writeFile(comparationFileName, data.getBuffer(), (err) => {                
            if (err) console.log(err);
            console.log(`Archivo creado en: ` + comparationFileName);       
        });
	});   
}

// node services/ResembleService.js public\\images\\T1-before.png public\\images\\T1-after.png

const execution = new Execution({
    insertionDate: new Date(),
    timestamp: timestamp,
    beforeImgUri: process.argv.slice(2)[0],
    afterImgUri: process.argv.slice(2)[1],
	comparationImgUri: process.argv.slice(2)[2]
});

ResembleService.compare(execution);

module.exports = ResembleService;