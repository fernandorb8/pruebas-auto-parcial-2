﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json.Linq;

namespace herramienta_pruebas_parcial_2
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        //public static string DIR_PROYECTO = @"C:\Users\fernando\git\pruebas-auto-parcial-2\";
        public static string DIR_PROYECTO = @"C:\Users\f.reyes948\git\pruebas-auto-parcial-2\";

        public static string DIR_SCREENSHOTS = @"screenshots\";

        public static string DIR_LOGS = @"logs\";

        public static string FILE_REPORTE = DIR_PROYECTO + @"reporte.html";

        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Ejecuta las pruebas de la aplicación baseline.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BaseLine(object sender, RoutedEventArgs e)
        {
            string carpetaLogs = DIR_PROYECTO + @"baseline\" + DIR_LOGS;
            crearDirsResultados(carpetaLogs);
            string logBDT = carpetaLogs + "BDT.log";
            string logRT = carpetaLogs + "RandomT.log";
            string pathApk = DIR_PROYECTO + @"baseline\" + "com.evancharlton.mileage_3110.apk";
            ejecutarBDT(pathApk, logBDT);
            ejecutarRT(logRT);
        }

        private void Mutantes(object sender, RoutedEventArgs e)
        {
            string logBDT;
            string logRT;
            string logVRT;
            string pathApk;
            int numeroMutante;
            foreach (string mdir in Directory.GetDirectories(DIR_PROYECTO + @"mutantes\"))
            {
                //Direcotorio de logs.
                crearDirsResultados(System.IO.Path.Combine(mdir, DIR_LOGS));

                //Inicializar variables.
                logBDT = "BDT.log";
                logRT = "RandomT.log";
                logVRT = "VRT.log";
                pathApk = "com.evancharlton.mileage_3110.apk";
                numeroMutante = int.Parse(System.IO.Path.GetFileName(mdir).Replace("com.evancharlton.mileage-mutant", ""));

                //Paths de archivos.
                logBDT = System.IO.Path.Combine(mdir, DIR_LOGS, logBDT);
                logRT = System.IO.Path.Combine(mdir, DIR_LOGS, logRT);
                logVRT = System.IO.Path.Combine(mdir, DIR_LOGS, logVRT);
                pathApk = System.IO.Path.Combine(mdir, pathApk);

                ejecutarBDT(pathApk, logBDT);

                ejecutarRT(logRT);

                //Ejecutar VRT.
                if (numeroMutante>=4395 && numeroMutante <= 4419)
                {
                    ejecutarVRT(mdir, logVRT);
                };
            }
        }

        /// <summary>
        /// Corrección de los mutantes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void apktool(object sender, RoutedEventArgs e)
        {
            string comando = "";
            string dir = "";
            string[] manifestContent;
            string manifestFile;
            string logFile = "apktool.log";
            foreach (string mdir in Directory.GetDirectories(DIR_PROYECTO + @"mutantes\"))
            {
                //Primer log.
                logToFile(logFile, "I: Procesando" + System.IO.Path.GetFileName(mdir));

                //Borrar descompilación vieja.
                borrarDirectorio(mdir + @"\com.evancharlton.mileage_3110\");

                //Descompilar.
                comando = @"java -jar ..\..\apktool_2.3.4.jar d com.evancharlton.mileage_3110.apk -f";
                dir = mdir;
                logToFile(logFile, "\n" + ejecutarComando(comando, dir));

                //Modificar Manifest.
                manifestFile = mdir + @"\com.evancharlton.mileage_3110\AndroidManifest.xml";
                manifestContent = File.ReadAllLines(manifestFile);
                using (StreamWriter sw = File.CreateText(manifestFile))
                {
                    sw.WriteLine(manifestContent[0]);
                    sw.WriteLine("\t<uses-permission android:name=\"android.permission.INTERNET\" />");
                    for (int i = 1; i < manifestContent.Length; i++)
                    {
                        sw.WriteLine(manifestContent[i]);
                    }
                }

                //Recompilar.
                comando = @"java -jar ..\..\apktool_2.3.4.jar b com.evancharlton.mileage_3110";
                dir = mdir;
                logToFile(logFile, "\n" + ejecutarComando(comando, dir));

                //Mover apk.
                File.Copy(mdir + @"\com.evancharlton.mileage_3110\dist\com.evancharlton.mileage_3110.apk"
                    , mdir + @"\com.evancharlton.mileage_3110.apk"
                    , true);
                logToFile(logFile, "\n" + "I: Apk copiada " + System.IO.Path.GetFileName(mdir));

                //Eliminar descompilado.
                borrarDirectorio(mdir + @"\com.evancharlton.mileage_3110\");
            }

            logToFile(logFile, "\n" + "I: Finaliza corrección con apktool");
        }

        /// <summary>
        /// Ejecuta un comando desde el directorio especificado.
        /// </summary>
        /// <param name="comando"></param>
        /// <param name="dir"></param>
        /// <returns></returns>
        public String ejecutarComando(String comando, String dir)
        {
            string path = dir.Equals("") ? "" : "cd /d " + dir + "&";

            ProcessStartInfo processStartInfo = new ProcessStartInfo("cmd", "/c " + path + comando);
            processStartInfo.RedirectStandardOutput = true;
            processStartInfo.RedirectStandardError = true;
            processStartInfo.WindowStyle = ProcessWindowStyle.Maximized;  //No tiene efecto tal vez por ser la cmd
            processStartInfo.CreateNoWindow = true; //Ocultar consola de comandos
            processStartInfo.UseShellExecute = false;

            Process process = new Process();
            StringBuilder processOutput = new StringBuilder("");
            StringBuilder processErrorOutput = new StringBuilder("");
            process.OutputDataReceived += new DataReceivedEventHandler(
                (sendingProcess, outLine) =>
                {
                    // Collect the sort command output.
                    if (!String.IsNullOrEmpty(outLine.Data))
                    {
                        // Add the text to the collected output.
                        processOutput.AppendLine(outLine.Data);
                        Console.WriteLine(outLine.Data);
                    }
                }
            );
            process.ErrorDataReceived += new DataReceivedEventHandler(
                (sendingProcess, outLine) =>
                {
                    // Collect the sort command output.
                    if (!String.IsNullOrEmpty(outLine.Data))
                    {
                        // Add the text to the collected output.
                        processErrorOutput.AppendLine(outLine.Data);
                        Console.WriteLine(outLine.Data);
                    }
                }
            );
            process.StartInfo = processStartInfo;
            process.Start();
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();
            process.WaitForExit();
            process.Dispose();

            byte[] bytes = Encoding.Default.GetBytes(processOutput.ToString() + " " + processErrorOutput.ToString());
            return Encoding.UTF8.GetString(bytes);
        }

        /// <summary>
        /// Registra en el log especificado.
        /// </summary>
        /// <param name="logName"></param>
        /// <param name="logLine"></param>
        public void logToFile(string logName, string logLine)
        {
            Console.WriteLine(logLine);
            using (StreamWriter sw = File.AppendText(logName))
            {
                sw.WriteLine(logLine);
            }
        }

        /// <summary>
        /// Borra un directorio de forma recursiva si existe.
        /// </summary>
        /// <param name="dir"></param>
        public void borrarDirectorio(string dir)
        {
            if (Directory.Exists(dir))
            {
                (new DirectoryInfo(dir)).Delete(true);
            }
        }

        /// <summary>
        /// Ejecuta las pruebas BDT para la APK especificada y guarda en el log especificado.
        /// </summary>
        /// <param name="apkPath"></param>
        /// <param name="logFilePath"></param>
        public void ejecutarBDT(string apkPath, string logFilePath)
        {
            string comando = "calabash-android resign " + apkPath;
            string dir = DIR_PROYECTO + @"calabash-android-files\";
            logToFile(logFilePath, ejecutarComando(comando, dir));

            comando = "calabash-android run " + apkPath;
            logToFile(logFilePath, ejecutarComando(comando, dir));

            string[] allFiles = System.IO.Directory.GetFiles(dir);
            foreach (string file in allFiles)
            {
                if (file.Contains("screenshot"))
                {
                    System.IO.File.Move(file
                        ,System.IO.Path.Combine(System.IO.Path.GetDirectoryName(logFilePath)
                        ,DIR_SCREENSHOTS, System.IO.Path.GetFileName(file)));
                }
                logToFile(logFilePath, "Imágenes copiadas para el mutante "
                    + System.IO.Path.GetFileName(System.IO.Path.GetDirectoryName(apkPath)));
            }

            logToFile(logFilePath, "Termina ejecución BDT para el mutante "
                + System.IO.Path.GetFileName(System.IO.Path.GetDirectoryName(apkPath)));
        }

        /// <summary>
        /// Genera un reporte html con los resultados de las ejecuciones.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void reporte(object sender, RoutedEventArgs e)
        {
            if (File.Exists(FILE_REPORTE))
            {
                File.Delete(FILE_REPORTE);
            }

            escribirEncabezadoReporte();

            string logBDT;
            string logRT;
            string logVRT;
            int numeroMutante;
            foreach (string mdir in Directory.GetDirectories(DIR_PROYECTO + @"mutantes-dev\"))
            {
                Console.WriteLine("Procesando logs de: " + mdir);

                //Inicializar variables.
                logBDT = "BDT.log";
                logRT = "RandomT.log";
                logVRT = "VRT.log";
                numeroMutante = int.Parse(System.IO.Path.GetFileName(mdir).Replace("com.evancharlton.mileage-mutant", ""));

                //Paths de archivos.
                logBDT = System.IO.Path.Combine(mdir, DIR_LOGS, logBDT);
                logRT = System.IO.Path.Combine(mdir, DIR_LOGS, logRT);
                logVRT = System.IO.Path.Combine(mdir, DIR_LOGS, logVRT);

                agregarElementoReporte("h1", "Mutante " + numeroMutante);

                bool falloBDT = false;
                bool falloRT = false;
                bool falloVRT = false;

                if (File.Exists(logBDT)) falloBDT = addBDTReport(logBDT, numeroMutante);
                if (File.Exists(logRT)) falloRT = addRTReport(logRT, numeroMutante);
                if (File.Exists(logVRT)) falloVRT = addVRTReport(logVRT, numeroMutante);

                if ((falloBDT || falloRT || falloVRT) == false)
                {
                    agregarElementoReporte("h2", "Ningún defecto encontrado");
                }
            }

            escribirFinalReporte();

            Console.WriteLine("Reporte generado");
        }

        /// <summary>
        /// Procesa un log de VRT.
        /// </summary>
        /// <param name="logVRT"></param>
        /// <param name="numeroMutante"></param>
        /// <returns></returns>
        private bool addVRTReport(string logVRT, int numeroMutante)
        {
            string[] reporte = File.ReadAllLines(logVRT);
            string linea = "";
            bool fallo = false;

            //Información VRT.
            double misMatchPercentage = 0;
            string basePath = "";
            string mutPath = "";
            string comPath = "";
            string[] parts = { };
            

            //Expresiones regulares.
            Regex rxPaths = new Regex(@"Comparando");
            Regex rxJson = new Regex(@"^{.*}$");
            Regex rxCompPath = new Regex(@"Archivo creado en: ");

            for (int i = 0; i < reporte.Length; i++)
            {
                linea = reporte[i];
                if (rxPaths.IsMatch(linea))
                {
                    parts = linea.Split(new char[] {' '});
                    mutPath = parts[1];
                    basePath = parts[2];
                } else if (rxJson.IsMatch(linea))
                {
                    JObject json = JObject.Parse(linea);
                    misMatchPercentage = (double)json["misMatchPercentage"];
                } else if (rxCompPath.IsMatch(linea))
                {
                    comPath = linea.Replace("Archivo creado en: ", "");
                    if (misMatchPercentage > 0)
                    {
                        fallo = true;
                        agregarElementoReporte("h2", "VRT - resemblejs");
                        agregarElementoReporte("h3", "Porcentaje de diferencia: " + misMatchPercentage);
                        agregarImagenReporte(basePath, "Baseline");
                        agregarImagenReporte(mutPath, "Mutante");
                        agregarImagenReporte(comPath, "Comparación");
                    } 
                }
            }

            return fallo;
        }

        /// <summary>
        /// Agrega una imagen al reporte. 
        /// </summary>
        /// <param name="pPath"></param>
        /// <param name="titulo"></param>
        private void agregarImagenReporte(string pPath, string titulo)
        {
            using (StreamWriter sw = File.AppendText(FILE_REPORTE))
            {
                sw.WriteLine("<h3>" + titulo + "</h3>");
                sw.WriteLine("<img src=\"" + pPath.Replace(DIR_PROYECTO, "") + "\" width=\"200\" height=\"355\">");
            }
        }

        /// <summary>
        /// Procesa un reporte RT.
        /// </summary>
        /// <param name="logRT"></param>
        /// <param name="numeroMutante"></param>
        /// <returns></returns>
        private bool addRTReport(string logRT, int numeroMutante)
        {
            string[] reporte = File.ReadAllLines(logRT);
            string linea = "";
            bool fallo = false;

            //Información RT.
            long seed = 0;
            int count = 0;
            int inyectados = 0;
            string error = "";

            //Expresiones regulares.
            Regex rxInfo = new Regex(@":Monkey: seed=(?<seed>\d+) count=(?<count>\d+)");
            Regex rxInyectados = new Regex(@"Events injected: (?<injected>\d+)");
            Regex rxError = new Regex(@"// Long Msg:");
            GroupCollection rxGroups;

            for (int i = 0; i < reporte.Length; i++)
            {
                linea = reporte[i];
                if (rxInfo.IsMatch(linea))
                {
                    rxGroups = rxInfo.Match(linea).Groups;
                    seed = long.Parse(rxGroups["seed"].Value);
                    count = int.Parse(rxGroups["count"].Value);
                } else if (rxInyectados.IsMatch(linea))
                {
                    rxGroups = rxInyectados.Match(linea).Groups;
                    inyectados = int.Parse(rxGroups["injected"].Value);
                } else if (rxError.IsMatch(linea))
                {
                    error = linea;
                    fallo = true;
                }                
            }
            if (fallo)
            {
                agregarElementoReporte("h2", "RT - adb monkey");
                agregarElementoReporte("h3", "Semilla: " + seed);
                agregarElementoReporte("h3", "Eventos a inyectar: " + count);
                agregarElementoReporte("h3", "Eventos inyectados: " + inyectados);
                agregarElementoReporte("h3", "Error: " + error);
            }

            return fallo;
        }

        /// <summary>
        /// Agrega un elemento al reporte html de tipo elem con contenido content.
        /// </summary>
        /// <param name="elem"></param>
        /// <param name="content"></param>
        private void agregarElementoReporte(string elem, string content)
        {
            using (StreamWriter sw = File.AppendText(FILE_REPORTE))
            {
                sw.WriteLine("<" + elem + ">" + content + "</" + elem + ">");
            }
        }

        /// <summary>
        /// Procesa un reporte BDT.
        /// </summary>
        /// <param name="logBDT"></param>
        /// <param name="numeroMutante"></param>
        private bool addBDTReport(string logBDT, int numeroMutante)
        {
            string[] reporte = File.ReadAllLines(logBDT);
            string linea = "";
            bool fallo = false;

            //Información BDT.
            string feature;
            string scenario;
            string error;
            string ubicacionError;

            //Expresiones regulares.
            Regex rx = new Regex(@"Feature:");
            Regex rxStep = new Regex(@"^\s*#?\s*(And|Given|When|Then)");
            Regex rxRes = new Regex(@"^\d+\sscenario");
            Regex rxFail = new Regex(@"Failing Scenarios:");
        
            for(int i = 0; i < reporte.Length; i++)
            {
                linea = reporte[i];
                if (rx.IsMatch(linea))
                {
                    feature = linea;
                    scenario = reporte[i + 1];
                    i += 2;
                    bool termino = false;
                    while (!termino)
                    {
                        linea = reporte[i];
                        if (rxStep.IsMatch(linea))
                        {
                            i++;
                        } else if (rxRes.IsMatch(linea) || rxFail.IsMatch(linea) || rx.IsMatch(linea))
                        {
                            termino = true;
                            i--;
                        } else
                        {
                            error = linea;
                            ubicacionError = reporte[i + 1];
                            agregarElementoReporte("h2", "BDT - calabash-android");
                            agregarElementoReporte("h3", feature);
                            agregarElementoReporte("h3", scenario);
                            agregarElementoReporte("h3", error);
                            agregarElementoReporte("h3", ubicacionError);
                            i += 2;
                            fallo = true;
                        }
                    }
                }
            }
            return fallo;
        }

        /// <summary>
        /// Escribe la última parte del reporte html.
        /// </summary>
        private void escribirFinalReporte()
        {
            using (StreamWriter sw = File.AppendText(FILE_REPORTE))
            {
                sw.WriteLine("	</body>" + "\n"
                    + "</html>");
            }
        }

        /// <summary>
        /// Escribe el encabezado del reporte html.
        /// </summary>
        private void escribirEncabezadoReporte()
        {
            using(StreamWriter sw = File.AppendText(FILE_REPORTE))
            {
                sw.WriteLine("<!DOCTYPE html>" + "\n"
                    + "<html>" + "\n"
                    + "    <head>" + "\n"
                    + "        <title> Reporte de pruebas </title>" + "\n"
                    + "    </head>" + "\n"
                    + "    <body> ");
            }
        }

        /// <summary>
        /// Ejecuta random testing y guarda en el log espeificado. Se debe ejecutar después de BDT.
        /// </summary>
        /// <param name="packageApk"></param>
        /// <param name="logRT"></param>
        public void ejecutarRT(string logRT)
        {
            string packageApk = "com.evancharlton.mileage";
            int numEventos = 1000000;
            string comando = "adb shell monkey -p " + packageApk + " -v " + numEventos;
            logToFile(logRT, ejecutarComando(comando, ""));

            logToFile(logRT, "Termina ejecución RT para el mutante "
                + System.IO.Path.GetFileName(System.IO.Path.GetDirectoryName(System.IO.Path.GetDirectoryName(logRT))));
        }

        /// <summary>
        /// Ejecuta VRT entre el mutante en mdir y el baseline.
        /// </summary>
        /// <param name="mdir"></param>
        /// <param name="logVRT"></param>
        private void ejecutarVRT(string mdir, string logVRT)
        {
            string baselineDirScreenshots = System.IO.Path.Combine(DIR_PROYECTO, @"baseline\", DIR_LOGS, DIR_SCREENSHOTS);
            string mutantDirScreenshots = System.IO.Path.Combine(mdir, DIR_LOGS, DIR_SCREENSHOTS);

            //Crear carpeta de comparación VRT y borrar carpetas anteriores.
            string vrtFinalFolder = System.IO.Path.Combine(mdir, DIR_LOGS, @"vrt\");
            Directory.CreateDirectory(vrtFinalFolder);

            string nombreImagen;
            string pathImagenBase;
            string comando;
            foreach(string pathImagenMut in Directory.GetFiles(mutantDirScreenshots))
            {
                nombreImagen = System.IO.Path.GetFileName(pathImagenMut);
                pathImagenBase = baselineDirScreenshots + nombreImagen;
                if (File.Exists(pathImagenBase))
                {
                    comando = "node services/ResembleService.js"
                        + " \"" + pathImagenMut + "\""
                        + " \"" + pathImagenBase + "\""
                        + " \"" + vrtFinalFolder + "\"";
                    logToFile(logVRT, "Comparando " + pathImagenMut + " " + pathImagenBase);
                    logToFile(logVRT, ejecutarComando(comando, System.IO.Path.Combine(DIR_PROYECTO, @"resemble-files\")));
                }
            }
            logToFile(logVRT, "Termina ejecución VRT para el mutante "
                + System.IO.Path.GetFileName(System.IO.Path.GetDirectoryName(System.IO.Path.GetDirectoryName(logVRT))));

        }

        /// <summary>
        /// Crea las carpetas de los logs y de los screenshots, borrando ejecuciones anteriores.
        /// </summary>
        /// <param name="carpetaLogs"></param>
        private void crearDirsResultados(string carpetaLogs)
        {
            if (Directory.Exists(carpetaLogs))
            {
                borrarDirectorio(carpetaLogs);
            }
            Directory.CreateDirectory(carpetaLogs);
            Directory.CreateDirectory(carpetaLogs + DIR_SCREENSHOTS);
        }
    }
}
