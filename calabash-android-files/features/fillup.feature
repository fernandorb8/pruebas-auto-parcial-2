Feature: Fillup feature

  Scenario: I can enter fillup information
    Given I enter "1200" into input field number 1
	And I enter "4" into input field number 2
	And I enter "1" into input field number 3
	#And I set the date to "01-01-2000" on DatePicker with index 1
	And I enter "First fillup" into input field number 4
	When I press the "Save Fillup" button 
    Then I see "1,200.00"
	And I take a screenshot
